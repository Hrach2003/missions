import React from 'react'
import c from './Translation.module.scss'
import { Header } from '../../components/Header'
import { WordsToTranslete } from '../../components/WordsToTranslate'

const wordsList = [
  {
    eng: 'between',
    rus: 'между',
    arm: 'մեջտեղում'
  },
  {
    eng: 'high',
    rus: 'высокий',
    arm: 'բարձր'
  },
  {
    eng: 'really',
    rus: 'действительно',
    arm: 'իրականում'
  },
  {
    eng: 'something',
    rus: 'что-нибудь',
    arm: 'ինչ-որ մի բան'
  },
  {
    eng: 'most',
    rus: 'большинство',
    arm: 'մեծամասնությունը'
  },
  {
    eng: 'another',
    rus: 'другой',
    arm: 'ուրիշ'
  },
  {
    eng: 'much',
    rus: 'много',
    arm: 'շատ'
  },
  {
    eng: 'family',
    rus: 'семья',
    arm: 'ընտանիք'

  },
  {
    eng: 'own',
    rus: 'личный',
    arm: 'անձնական'

  },
  {
    eng: 'out',
    rus: 'из/вне',
    arm: 'դուրս'

  },
  {
    eng: 'leave',
    rus: 'покидать',
    arm: 'լքել'
  },
];

export const TranslationPage = () => {
  return (
    <div className={c.trPage}>
      <Header
        color="rgb(0, 140, 50)"
        noShadow
        title="“The limits of my language mean the limits of my world.”"
        desc="– Ludwig Wittgenstein"
      />
      <WordsToTranslete wordsList={wordsList} />
    </div>
  )
}

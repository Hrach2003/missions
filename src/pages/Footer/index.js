import React from 'react'
import c from './footer.module.scss'
import { Header } from '../../components/Header'

export const Footer = () => {
  return (
    <div className={c.footer}>
      <Header
        desc={
          <>
            <i className="fas fa-copyright" /> {" "}
            Powered by React-ивный марафон
          </>
        }
      />
    </div>
  )
}

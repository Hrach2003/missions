import React from 'react'
import c from './welcome.module.scss'

export const Welcome = ({ children }) => {
  return (
    <div className={c.welcomePage}>
      <div className={`${c.clipImg} ${c.center}`}>
        {children}
      </div>
      <div className={c.container}>
        <div className={c.scrollBottom}>
          <i className="fas fa-chevron-down" />
        </div>
      </div>
    </div>
  )
}

import React from 'react';
import { Welcome } from './pages/WelcomePage';
import { Header } from './components/Header';
import { TranslationPage } from './pages/WordTranslationPage';
import { Footer } from './pages/Footer';

function App() {
  return (
    <div>
      <Welcome>
        <Header
          title="Welcome to the present moment"
          desc="Here. Now. The only moment there ever is."
        />
      </Welcome>
      <TranslationPage />
      <Footer />
    </div>
  );
}

export default App;

import React from 'react'
import c from './Header.module.scss'

export const Header = ({ title, desc, color, noShadow = false, ...props }) => {
  let classes = `${c.text} ${!noShadow ? c.shadow : ''}`
  return (
    <div style={{ textAlign: 'center', maxWidth: '80%', margin: '0 auto' }} {...props} >
      <h1 style={{ color }} className={classes}>{title}</h1>
      <p style={{ color }} className={classes}>{desc}</p>
    </div>
  )
}

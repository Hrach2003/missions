import React, { useState, useRef } from 'react';
import s from './card.module.scss';
import cl from 'classnames'

export const Card = ({ eng, rus, arm, id }) => {
  const [active, setActive] = useState(false)
  const slideDown = useRef()
  const toggleSlide = () => {
    active
      ? slideDown.current.style.maxHeight = 0
      : slideDown.current.style.maxHeight = `${slideDown.current.scrollHeight}px`;
    setActive(a => !a)
  }
  return (
    <div className={cl(s.card, { [s.reversed]: active })} onClick={toggleSlide}>
      <div className={s.cardInner}>
        <div>
          {eng}
        </div>
        <div>
          <i className={`fas fa-${active ? 'minus' : 'plus'}`} />
        </div>
      </div>

      <div
        ref={slideDown}
        style={{
          transition: "max-height 0.3s ease-out",
          overflow: "hidden",
          maxHeight: 0,
        }}
      >
        <div>
          <span>Russian:</span> {rus}
        </div>
        <div>
          <span>Armenian:</span> {arm}
        </div>
      </div>
    </div>
  );
}
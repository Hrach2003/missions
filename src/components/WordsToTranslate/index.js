import React from 'react'
import c from './wordsToTranslate.module.scss'
import { Card } from './Card';



export const WordsToTranslete = ({ wordsList }) => {
  return (
    <div className={c.containerGrid}>
      {wordsList.map((word, i) => {
        return (
          <div className={c.center} key={i}>
            <Card rus={word.rus} eng={word.eng} arm={word.arm} id={i} />
          </div>
        )
      })}
    </div>
  )
}
